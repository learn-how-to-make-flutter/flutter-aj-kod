import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_aj_kod/bmi_screen.dart';
import 'package:flutter_aj_kod/cal_page.dart';
import 'package:flutter_aj_kod/services/user_service.dart';
import 'package:google_fonts/google_fonts.dart';

import 'models/user.dart';

class Bim_Page extends StatefulWidget {
  const Bim_Page({Key? key}) : super(key: key);

  @override
  State<Bim_Page> createState() => _Bim_PageState();
}

class _Bim_PageState extends State<Bim_Page> {
  User? user;

  @override
  void initState() {
    super.initState();
    getUser();
  }

  Future<void> getUser() async {
    var userTmp = await UserService.loadUser();
    setState(() {
      user = userTmp;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.blue,
          child: Image.asset(
            "assets/images/man0.jpg",
            fit: BoxFit.cover,
          ),
        ),
        Container(
          color: Colors.white.withOpacity(0.5),
          margin:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.7),
          padding: EdgeInsets.all(16),
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width,
          child: Text(
            "I am open to new ways to take better care of my health.",
            style: GoogleFonts.niramit(
              color: Colors.white,
              fontSize: 35,
              fontWeight: FontWeight.bold,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        Column(
          children: [
            Expanded(
              child: Container(),
            ),
            Container(
              height: 70,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Text(
                      "BMI",
                      style: GoogleFonts.niramit(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      // Navigator.of(context).pushNamed(Cal_Pange.tag);
                      if (user == null) {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: ((context) {
                              return Cal_Pange();
                            }),
                          ),
                        );
                      } else {
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: ((context) {
                              return BmiScreen(user: user!);
                            }),
                          ),
                        );
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.blue,
                      ),
                      margin: EdgeInsets.all(5),
                      width: 50,
                      height: 50,
                      child: Icon(Icons.east_outlined),
                    ),
                  )
                ],
              ),
            ),
          ],
        )
      ]),
    );
  }
}
