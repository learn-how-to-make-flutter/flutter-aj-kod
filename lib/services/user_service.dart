import 'package:shared_preferences/shared_preferences.dart';

import '../models/user.dart';

class UserService {
  static Future<User?> loadUser() async {
    var prefs = await SharedPreferences.getInstance();
    var userJson = prefs.getString("user");
    if (userJson == null) {
      return null;
    }
    return User.fromJson(userJson);
  }

  static Future<void> saveUser(User user) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString("user", user.toJson());
  }

  static Future<void> clearUser() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.remove("user");
    prefs.remove("bmi");
  }
}
