import 'dart:convert';

import '../models/bmi_data.dart';

void main(List<String> args) {
  var bmiList = <BmiData>[
    BmiData.newBmi(weight: 81, height: 181),
    BmiData.newBmi(weight: 82, height: 181),
    BmiData.newBmi(weight: 83, height: 181),
    BmiData.newBmi(weight: 85, height: 181),
    BmiData.newBmi(weight: 86, height: 181),
    BmiData.newBmi(weight: 87, height: 181),
    BmiData.newBmi(weight: 89, height: 181),
    BmiData.newBmi(weight: 90, height: 181),
    BmiData.newBmi(weight: 91, height: 181),
    BmiData.newBmi(weight: 92, height: 181),
    BmiData.newBmi(weight: 93, height: 181),
    BmiData.newBmi(weight: 94, height: 181),
    BmiData.newBmi(weight: 90, height: 181),
    BmiData.newBmi(weight: 89, height: 181),
    BmiData.newBmi(weight: 88, height: 181),
    BmiData.newBmi(weight: 87, height: 181),
    BmiData.newBmi(weight: 86, height: 181),
  ];

  var b1 = BmiData.newBmi(weight: 81, height: 181);
  print("${b1.weight} ${b1.height} ${b1.logTime} ${b1.bmi}");

  var strJsonList = json.encode(bmiList);
  print(strJsonList);
  var bmis = json.decode(strJsonList).map((value) {
    return BmiData.fromJson(value);
  }).toList();
  print(bmis);
}
